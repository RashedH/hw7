#!/bin/bash
echo -e "Hangman - The unix version of the hangman game"
retval=-1
guesses=0
correct_guesses=0
SECRET_WORD=`cat words.txt | shuf -n 1`
SECRET_WORD=${SECRET_WORD^^}
GUESSED_WORD="${SECRET_WORD//[A-Z]/_}"
MAX_GUESSES=`expr 2 \* ${#SECRET_WORD}`
function print_secret_word() {
    word=$SECRET_WORD
    guessed=$GUESSED_WORD
    for (( i=0; i<${#guessed}; i++ )); do
        echo -n "${guessed:$i:1}"
    done
}
# function to check if letter is in unguessed letters of the secret word
function letter_is_in_unguessed_letters_of_word() {
    word=$1
    letter=$2
    retval=-1
    for (( i=0; i<${#word}; i++ )); do
        if [ ${word:$i:1} = $letter ] && [ ${GUESSED_WORD:$i:1} == "_" ] ; then
            retval=$i
            break
        fi
    done
}
while [ $guesses -lt $MAX_GUESSES ]
do
    print_secret_word
    echo -e "\nGuess a letter: "
    read letter
    letter="${letter^^}"
    letter_is_in_unguessed_letters_of_word $SECRET_WORD $letter
    if [ "$retval" != -1 ] ; then
        GUESSED_WORD=`echo "$GUESSED_WORD" | sed "s/\(.\{$retval\}\)./\1$letter/"`
        correct_guesses=$((correct_guesses+1))
    fi
    guesses=$((guesses+1))
    if [ "$correct_guesses" == ${#SECRET_WORD} ] ; then
        break;
    fi
done

if [ "$correct_guesses" == ${#SECRET_WORD} ] ; then
    echo "You won! Congratulations!"
else
    echo "Oops! You lost!"
fi

